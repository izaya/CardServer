/**
 * Created by Francis Yang on 7/15/17.
 */
// request object storing 2 hashmaps, message and timestamp
function Request () {
	if (false === (this instanceof Request)) {
		return new Request();
	}
	this.msg = new Map ();
	this.timestamp = new Map ();
}

Request.prototype.constructor = Request;

Request.prototype.addRequest = function (_requestId, _msg) {
	this.msg.requestId = _msg;
	this.timestamp.requestId = Date.now();
};

Request.prototype.getRequestById = function (_requestId) {
	return {
		msg: this.msg [_requestId],
		timestamp: this.timestamp [_requestId]
	};
};

module.exports = Request;