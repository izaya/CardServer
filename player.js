/**
 * Created by Francis Yang on 6/30/17.
 */
let Request = require ('./request.js');
const OFFLINE = -1;

function Player (_id) {
    if (false === (this instanceof Player)) {
        return new Player ();
    }
    this.userId = _id;
    this.cards = {};
    this.state = OFFLINE;
    this.requests = new Request ();
    this.requestID = 0;
    this.health = 20;
    this.price = 1;
    this.socketId = null;
    this.otherID = null;        // socket id of opponent
	this.gameID = null;     // current game id
}

Player.prototype.constructor = Player;

// clear data after play
Player.prototype.resetGame = function () {
    this.userId = null;
    this.cards = {};
    this.state = OFFLINE;
    this.requests = null;
    this.requestID = 0;
    this.health = null;
    this.price = null;
    this.socketId = null;
    this.otherID = null;
    this.gameID = null;
};

// An expensive function, only used for initializing 4 cards at the beginning and unit test
Player.prototype.getCards = function () {
	let array = [];
	for (let key in this.cards) {
		if (this.cards.hasOwnProperty (key)) {
			array.push (this.cards [key]);
		}
	}
    return array;
};

Player.prototype.getState = function () {
    return this.state;
};

Player.prototype.getPrice = function () {
	return this.price;
};

Player.prototype.getHealth = function () {
	return this.health;
};

Player.prototype.getGameID = function () {
	return this.gameID;
};

Player.prototype.setState = function (val) {
	this.state = val;
};

Player.prototype.setPrice = function (val) {
	this.price = val;
};

Player.prototype.setGameID = function (val) {
	this.gameID = val;
};

Player.prototype.setHealth = function (val) {
	this.health = val;
};

Player.prototype.sabotage = function (val) {
	this.health -= val;
	return this.health;
};

Player.prototype.insert = function (_id, card) {
    this.cards[_id] = card;
};

Player.prototype.pushRequest = function (msg) {
	this.requests.addRequest (this.requestID, msg);
	this.requestID++;
};

Player.prototype.getRequestCount = function () {
	return this.requestID + 1;
};

Player.prototype.getRequests = function () {
	return this.requests;
};

Player.prototype.setSocketId = function (_id) {
	this.socketId = _id;
};

Player.prototype.getSocketId = function () {
	return this.socketId;
};

Player.prototype.setOtherID = function (_id) {
	this.otherID = _id;
};

Player.prototype.getOtherID = function () {
	return this.otherID;
};

Player.prototype.getCardById = function (_id) {
	if (this.cards [_id] !== undefined) {
		return this.cards [_id];
	}
	return null;
};

Player.prototype.getCardDamage = function (_id) {
	if (this.cards[_id] !== undefined) {
		return this.cards [_id].getDamage ();
	}
	return null;
};

Player.prototype.getCardState = function (_id) {
	if (this.cards [_id] !== undefined) {
		return this.cards [_id].getState ();
	}
	return null;
};

Player.prototype.sabotageCard = function (_id, val) {
	if (this.cards [_id] !== undefined) {
		this.cards [_id].sabotage (val);
		return this.cards [_id].getHealth ();
	}
	return null;
};

Player.prototype.setCardState = function (_id, _state) {
	if (this.cards [_id] !== undefined) {
		this.cards [_id].changeState (_state);
		return true;
	}
	return false;
};

module.exports = Player;