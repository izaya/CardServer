/**
 * Created by Francis Yang on 6/16/17.
 */
let Game = require ('./game.js');
let Player = require ('./player.js');
let Card = require ('./card.js');
let crypto = require ('crypto');

// player constant states
const GAMEOVER = -1;
const ONLINE = 0;
const CONNECTED = 1;
const ENEMYTURN = 2;
const MYTURN = 3;
const WIN = 4;
const LOSE = 5;
const OFFLINE = -1;

// card constant states
const INDECK = 0;
const INHAND1 = 1;      // in hand, unable to play
const INHAND2 = 2;      // in hand, able to play
const ONTABLE1 = 3;     // on table, unable to attack
const ONTABLE2 = 4;     // on table, able to attack
const DESTROIED = -1;

let games = {};     // pool of games, namespace as key
let players = {};   // pool of players, userId as key
let playerID = {};  // dictionary stores user ID. Socket ID as key.

let app = require ('express') ();
let server = require ('http').Server (app);
let io = require ('socket.io') (server);

const PORT = 8080;
const HOST = '0.0.0.0';

server.listen (PORT, HOST, function () {
	console.log (`Running on http://${HOST}:${PORT}`);
});

/**
 * Testing client
 */
app.get ('/', (req, res) => {
	let message = process.env.MESSAGE || 'Hello, World';
	let contentType = process.env.CONTENT_TYPE || 'text/plain';

	res.statusCode = 200;
	res.setHeader ('Content-Type', contentType);
	res.end (message + '\n');
});

app.get ('/index', function (req, res) {
	res.sendFile (__dirname + '/index.html');
});

/**
 * Begin of RPC service
 */
io.on ('connection', function (socket) {
    // create a new player with the new user ID
	if (playerID [socket.id] === undefined) {
        let userId = Date.now () % 100000000;
		players [userId] = new Player (userId);
		players [userId].setSocketId (socket.id);
		playerID [socket.id] = userId;
        players [userId].setState (ONLINE);
        players [userId].pushRequest ('connection');
		/**
		 * 1. Connection
		 */
		let data = {
			userId:             userId,
			userState:        ONLINE
		};
		// First EMIT
		socket.emit ('connectionConfirmed', JSON.stringify (data));

		console.log ("User " + userId + " connected");
		console.log ("Socket ID: " + players [userId].getSocketId ());
	}

    socket.on ('disconnect', function () {
        if (playerID [socket.id] === undefined) {
        	console.log ("Player ID map does not have id " + socket.id + " for disconnect");
            return;
        }
    	let _userId = playerID [socket.id];
        players [_userId].pushRequest ('disconnect');
        console.log ("Player "+ _userId + " has disconnected");

    	players [_userId].setState(OFFLINE);
		let gameID = players [_userId].getGameID ();

		// Game is not finished; The other player WIN
    	if (games [gameID] !== undefined && games [gameID].getState () !== GAMEOVER) {
            let otherClientID = players [_userId].getOtherID();
            let otherPlayerID = playerID [otherClientID];

            let data = {
                userId:             otherPlayerID,
                userState:       WIN
            };
            players [otherPlayerID].setState (WIN);
            games [gameID].setState (GAMEOVER);

            io.to (otherClientID).emit ('gameOver', JSON.stringify (data));
            console.log ("Player " + otherPlayerID + " won the game!");
	    } else {
            delete games [gameID];
	    }

	    // remove ID entry from hash maps or objects
    	delete playerID [socket.id];
    	delete players [_userId];
    });

	/**
	 * 2. Match making
	 */
    socket.on ('matchMaking', function (_requestId, _userId) {
        if (playerID [socket.id] === undefined) {
        	console.log ("Players map does not have user with id " + _userId + " for matchMaking");
            return;
        }
	    players [_userId].pushRequest ('matchMaking');
	    console.log ("Match making request from " + _userId);

	    // get opponent socket id
	    for (let clientId in io.sockets.connected) {
	    	if (clientId === socket.id)
	    		continue;

		    let otherPlayerID = playerID [clientId];
		    if (players [otherPlayerID].getState () !== ONLINE)
			    continue;

		    // Match made. Set the other player's clientId for both players
		    players [_userId].setOtherID (clientId);
		    players [otherPlayerID].setOtherID (socket.id);

		    // make new game instance by the game id generated
		    let matchId = makeGameId (_userId, otherPlayerID);
		    games [matchId] = new Game (matchId);

		    if (games [matchId] === null) {
		    	console.log ("Null match id for matchMaking");
		    	continue;
		    }

		    games [matchId].init ();
		    players [_userId].setGameID (matchId);
		    players [otherPlayerID].setGameID (matchId);
            players [_userId].setState (CONNECTED);
            players [otherPlayerID].setState (CONNECTED);

		    let data = {
			    userId:             _userId,
			    userState:       CONNECTED,
			    gameId:         matchId
		    };
		    socket.emit ('matchMade', JSON.stringify (data));

		    // emit data to the other socket client
		    let data2 = {
			    userId:            otherPlayerID,
			    userState:      CONNECTED,
			    gameId:         matchId
		    };
		    io.to (clientId).emit ('matchMade', JSON.stringify (data2));

		    // set the first turn of the other player because they are the first
		    games [matchId].setState (otherPlayerID);

		    console.log ("Other player ID is " + otherPlayerID);
		    console.log ("Get other client ID is " + players [_userId].getOtherID ());
		    console.log ("Match ID is " + matchId);
		    break;
	    }
    });

	/**
	 * 3. Initialization
	 */
    socket.on ('gameInit', function (_requestId, _userId, _gameId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for gameInit");
            return;
        }
        if (games [_gameId] === undefined) {
            console.log ("Game with id " + _userId + " is not made" + " for gameInit");
            return;
        }
    	// decide whether it's the first turn by game state
	    if (games [_gameId].getState () === _userId) {
		    players [_userId].setState (MYTURN);
		    socket.userState = MYTURN;
	    } else {
		    players [_userId].setState (ENEMYTURN);
		    socket.userState = ENEMYTURN;
	    }

	    players [_userId].pushRequest ('gameInit');
	    // type, health, damage, price, title, des, id
	    players [_userId].insert (_userId % 10000, new Card (1, 1, 1, 0, "SCV", "Light unit used to construct buildings, gather resources, and perform repairs.", _userId % 10000));
	    players [_userId].insert (_userId % 10000 + 1, new Card (2, 2, 1, 1, "MARINE", "Well-rounded unit suited for a wide range of combat situations.", _userId % 10000 + 1));
	    players [_userId].insert (_userId % 10000 + 2, new Card (3, 1, 2, 1, "REAPER", "Very fast raider that can jump over ledges, which also afford them great mobility.", _userId % 10000 + 2));
	    players [_userId].insert (_userId % 10000 + 3, new Card (4, 2, 2, 2, "MARAUDER", "Armored assault trooper that can attack ground targets.", _userId % 10000 + 3));

        let data = {
        	userId:             _userId,
	        userState:        socket.userState,
        	cards:              players [_userId].getCards ()
        };

        socket.emit ('initialized', JSON.stringify (data));
        console.log ("Game Initialized");
    });

	/**
	 * 4. Turn End
	 */
	socket.on ('turnEnd', function (_requestId, _userId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for turnEnd");
            return;
        }
		players [_userId].pushRequest ('turnEnd');

		// stop player from drawing or playing cards
		players [_userId].setState (ENEMYTURN);

		// start turn of the other player and find the other player id of the other socket client id
		let otherClientId = players [_userId].getOtherID ();
		let otherPlayerId = playerID [otherClientId];
		players [otherPlayerId].setState (MYTURN);

		// server side turn recorder
		let matchId = players [_userId].getGameID();
		games [matchId].setState (otherPlayerId);

		// increase hand price each new turn before 10
		let handPrice = players [otherPlayerId].getPrice ();

		if ( handPrice < 10 ) {
			handPrice++;
			players [otherPlayerId].setPrice (handPrice);
		} else {
			handPrice = 10;
		}

		console.log ("Player " + _userId + " turn end");

		// emit data to the other socket client
		let drawnCard = games [matchId].pop ();

		if (drawnCard !== null) {
			players [otherPlayerId].insert (drawnCard.getId (), drawnCard);
		} else {
			console.log ("No card in game " + matchId + " for player " + otherPlayerId + " for drawCard");
		}

		let data = {
			userId:             otherPlayerId,
			userState:       MYTURN,
			newCard:       drawnCard,
			price:              handPrice
		};

		io.to (otherClientId).emit ("startTurn", JSON.stringify (data));
		console.log ("Player " + otherPlayerId + " turn start with price of " + handPrice);
	});

	/**
	 * 5. Draw a Card
	 */
	socket.on ('drawCard', function (_requestId, _userId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for drawCard");
            return;
        }
		players [_userId].pushRequest ('drawCard');

		let matchId = players [_userId].getGameID ();
		let drawnCard = games [matchId].pop ();

		if (drawnCard !== null) {
			players [_userId].insert (drawnCard);
		} else {
			console.log ("No card in game " + matchId + " for player " + _userId + " for drawCard");
		}

		let data = {
			userId:         _userId,
			userState:   MYTURN,
			newCard:   drawnCard
		};

		socket.emit ('newCard', JSON.stringify (data));
		console.log ("player " + _userId + " draw a card");
	});

	/**
	 * 6. Play a Card
	 */
	socket.on ('playCard', function (_requestId, _userId, _playCardId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for playCard");
            return;
        }
		players [_userId].pushRequest ('playCard');
		let playCard = players [_userId].getCardById (_playCardId);

		if (playCard === null) {
			console.log ("Couldn't find card " + _playCardId + " in player " + _userId + " for playCard");
			return;
		}
		players[_userId].setCardState (_playCardId, ONTABLE1);

		let otherClientId = players [_userId].getOtherID ();
		let otherPlayerId = playerID [otherClientId];

		let data = {
			userId:                         otherPlayerId,
			enemyPlayedCard:    playCard
		};

		io.to (otherClientId).emit ('updateTable', JSON.stringify (data));
		console.log ("Card " + _playCardId + " is played on table by player " + _userId + "; state: " + players [_userId].getCardState (_playCardId));
	});

	/**
	 * 7. Attack a hero
	 */
	socket.on ('attackHero', function (_requestId, _userId, _attackerId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for attackHero");
            return;
        }
		players [_userId].pushRequest ('playCard');
		let playCard = players [_userId].getCardById (_attackerId);

		if (playCard === null) {
			console.log ("Couldn't find attacker card " + _attackerId + " in player " + _userId + " for attackHero");
			return;
		}

		players [_userId].setCardState (_attackerId, ONTABLE1);      // state 3: on table, unable to attack

		let otherClientId = players [_userId].getOtherID ();
		let otherPlayerId = playerID [otherClientId];

		// new health of the enemy player
		let newHealth = players [otherPlayerId].sabotage (playCard.getDamage ());

		// Game Over check
		if (newHealth <= 0) {
			let data = {
				userId:             _userId,
				userState:       WIN
			};
            players [_userId].setState (WIN);
			socket.emit ('gameOver', JSON.stringify (data));

			let data2 = {
				userId:         otherPlayerId,
				userState:    LOSE
			};
            players [otherPlayerId].setState (LOSE);
			io.to (otherClientId).emit ('gameOver', JSON.stringify (data2));

			let gameID = players [_userId].getGameID();
			games [gameID].setState (GAMEOVER);

			console.log ("Game over. Winner is " + _userId);
			return;
		}

		let data = {
			attacker:           _userId,
			victim:             otherPlayerId,
			attackerId:      _attackerId,
			heroHealth: newHealth
		};
		// Broadcast the new hero health to all clients
		socket.emit ('updateHero', JSON.stringify (data));
		io.to (otherClientId).emit ('updateHero', JSON.stringify (data));
		console.log ("Hero " + otherPlayerId + " is under attack. New health is " + newHealth);
	});

	/**
	 * 8. Attack a card
	 */
	socket.on ('attackCard', function (_requestId, _userId, _attackerId, _victimId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for attackCard");
            return;
        }
		players [_userId].pushRequest ('attackCard');
		let attackDamage = players [_userId].getCardDamage (_attackerId);

		if (attackDamage === null) {
			console.log ("Couldn't find attacker card " + _attackerId + " in player " + _userId + " for attackCard");
			return;
		}
		players[_userId].setCardState (_attackerId, ONTABLE1);      // state 3: on table, unable to attack

		// get the damage of the victim card of the other player
		let otherClientId = players [_userId].getOtherID ();
		let otherPlayerId = playerID [otherClientId];
		let victimDamage = players [otherPlayerId].getCardDamage (_victimId);

        if (victimDamage === null) {
            console.log ("Couldn't find victim card " + _victimId + " in player " + otherPlayerId + " for attackCard");
            return;
        }

		// calculate updated health to each other after sabotage; -1 to destroy
		let attackCardHP = players [_userId].sabotageCard (_attackerId, victimDamage);
		let victimCardHP = players [otherPlayerId].sabotageCard (_victimId, attackDamage);

		let data = {
			attacker:           _userId,
			victim:              otherPlayerId,
			attackerCard:  players [_userId].getCardById (_attackerId),
			victimCard:     players [otherPlayerId].getCardById (_victimId)
		};
		io.to (otherClientId).emit ('updateCards', JSON.stringify (data));
        console.log ("Attacker card id " + _attackerId + "; health: " + attackCardHP);
        console.log ("Victim card id " + _victimId + "; health: " + victimCardHP);
	});

	/**
	 * 9. Get cards for testing
	 */
	socket.on ('getHand', function (_requestId, _userId) {
        if (playerID [socket.id] === undefined) {
            console.log ("Players map does not have user with id " + _userId + " for getHand");
            return;
        }
		let data = {
			userId:             _userId,
			cards:              players [_userId].getCards ()
		};
		socket.emit ('updateCards', JSON.stringify (data));
	});
});

/**
 * Return a slice of the last 6 characters of the hash as game id
 */
function makeGameId (val1, val2) {
	let hash = crypto.createHash ('md5').update (Date.now ().toString () + val1.toString () + val2.toString ()).digest ('hex');
	return hash.slice (hash.length - 6, hash.length);
}
