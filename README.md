# Card Server Version 1.0#
A game server for my online card game [CardIO] (https://gitlab.com/izaya/CardIO) written in **NodeJS** using [Socket.io] (https://socket.io/). Socket.io version is 2.0.3 and NodeJS version is 8.1.3.  
For more information of Socket.io APIs, check out [https://socket.io/docs/](https://socket.io/docs/)
# Setup #
```
npm install
```
# Project Structure #
- index.js        -- http server
- game.js         -- card deck
- player.js       -- player hand
- card.js         -- card data
- request.js         -- request log data
- package.json    -- dependencies

# Server APIs #
To describe the request and responses, I am using a format similar to [JSON-RPC 2.0 specifications](http://www.jsonrpc.org/specification) customized for Socket.io.  
An example of emitting a response (not exactly the value namings I am using):
```
socket.on ('gameInit', function (userId, request_id) {
    var res = {
        requestId: request_id
        userId: userId,
	    userState: players [userId].userState,
    	cards: game.get()
    };

    socket.emit ('Initialized', res);
    console.log ('Game Initialized');
}
```

### 1. Connection
```
--> {"method": "connection", "params": null }
<-- {"method": "connectionConfirmed", "result": {"userId": socket.userId, "userState": socket.userState } }
```
### 2. Match making
```
--> {"method": "matchMaking", "params": {"requestId": socket.requestId, "userId": socket.userId } }
<-- {"method": "matchMade", "result": {"userId": socket.userId, "userState": socket.userState, "gameId": socket.gameId } }
```
### 3. Initialization
```
--> {"method": "gameInit", "params": {"requestId": socket.requestId, "userId": socket.userId, "gameId": socket.gameId } }
<-- {"method": "initialized", "result": {"userId": socket.userId, "userState": socket.userState, "cards": game [gameId].get() } }
```
### 4. Turn End
```
--> {"method": "turnEnd", "params": {"requestId": socket.requestId, "userId": socket.userId} }
```
To the other client
```
<-- {"method": "startTurn", "result": { "userId": socket.userId, "userState": socket.userState, "newCard": socket.newCard, "price": socket.price } }
```
### 5. Play a Card
```
--> {"method": "playCard", "params": {"requestId": socket.requestId, "userId": socket.userId, "playCardId": hand.cards[id].id} }
```
Notify the other player a new card should be displayed on the table
```
<-- {"method": "updateTable", "result": { "userId": socket.otherPlayerId, "enemyPlayedCard": players[userId].cards[id] } }
```
### 6. Attack a Card
```
--> {"method": "attackCard", "params":  {"requestId": socket.requestId, "userId": socket.userId, "attackerId": hand.cards[id].id,  "victimId": table.cards[id].id} }
```
Broadcast the new card information to all clients
```
<-- {"method": "updateCards", "result": {"attacker": attackerId, "victim": victimId, "attackerCard": players[userId].cards[id],  "victimCard": players[userId].cards[id]} }
```
### 7. Attack a Hero
```
--> {"method": "attackHero", "params":  {"requestId": socket.requestId, "userId": socket.userId, "attackerId": hand.cards[id].id} }
```
Broadcast the new hero health to all clients
```
<-- {"method": "updateHero", "result": {"attacker": attackerId, "victim": victimId, "attackerId": hand.cards[id].id, "heroHealth": newhealth} }
```
Check winner. If the game is over, send a notification.
```
<-- {"method": "gameOver", "result": {"userId": socket.userId, "userState": socket.userState } }
```
### 8. Disconnect
```
--> {"method": "disconnect", "params": {"requestId": socket.requestId, "userId": socket.userId } }
```
# Deserialize Responses #
An example implementation of C# in Unity.
```
public class CardData {
	public int id;
	public int type;
	public int health;
	public int damage;
	public int state;
};

public class Result {
	public int requestId;
	public int userId;
	public int userState;
	public int playCardState;
	public int attackerId;
	public int victimId;
	public CardData[] cards;
	public CardData newCard;
	public CardData attackerCard;
	public CardData victimCard;
};

socket.On ("connectionConfirmed", (data) => {
     Debug.Log ("Connection Confirmed");
	    // deserialize received JSON data
	    Result res = JsonUtility.FromJson<Result>(data.ToString ());
	    userId = res.userId;
	    userState = res.userState;
	    // ...
	    socket.Emit ("matchMaking", requestID, userId);
	    requestID++;
});
```
# Screenshot #
This shows an example of using the jQuery testing environment and back end server log in terminal, IDE or Docker log to track events.
<img src="/test.png">
# TODO #
Benchmark ping timeout latency; Player file, request log and replay; database support; reset and keep the player online after playing a game; reconnection and mobile network