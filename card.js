/**
 * Created by Francis Yang on 6/30/17.
 */
const INDECK = 0;
const INHAND1 = 1;      // in hand, unable to play
const INHAND2 = 2;      // in hand, able to play
const ONTABLE1 = 3;     // on table, unable to attack
const ONTABLE2 = 4;     // on table, able to attack
const DESTROIED = -1;

function Card (type, health, damage, price, title, des, id) {
    if (false === (this instanceof Card)) {
        return new Card ();
    }
    this.id = id;
    this.type = type;
    this.health = health;
    this.damage = damage;
	this.price = price;
    this.title = title;
    this.des = des;
	this.state = INHAND1;
}

Card.prototype.constructor = Card;

Card.prototype.getId = function () {
	return this.id;
};

Card.prototype.getType = function () {
	return this.type;
};

Card.prototype.getState = function () {
	return this.state;
};

Card.prototype.getPrice = function () {
	return this.price;
};

Card.prototype.getDamage = function () {
	return this.damage;
};

Card.prototype.getHealth = function () {
	return this.health;
};

Card.prototype.setId = function (_id) {
	this.id = _id;
};

Card.prototype.changeState = function (_state) {
	if (this.state === DESTROIED)
		return;
	this.state = _state;
};

Card.prototype.sabotage = function (val) {
	if (this.state === DESTROIED)
		return;
	this.health = this.health - val;

	if (this.health <= 0) {
		this.health = DESTROIED;
		this.state = DESTROIED;
	}
	return this.health;
};

module.exports = Card;