/**
 * Created by Francis Yang on 6/30/17.
 */
let Card = require("./card.js");
const GAMEOVER = -1;

function Game (_id) {
    if (false === (this instanceof Game)) {
        return new Game ();
    }
    this.id = _id;
    this.cards = [];       // card deck
    this.state = GAMEOVER;      // state -1: init or game over, not playable; otherwise equals to player id
}

Game.prototype.constructor = Game;

Game.prototype.get = function () {
    return this.cards;
};

Game.prototype.pop = function () {
	if (this.cards.length > 0) {
		return this.cards.pop ();
	}
    return null;
};

Game.prototype.setState = function (newState) {
	this.state = newState;
};

Game.prototype.getState = function () {
	return this.state;
};

// create cards into the deck and shuffle
Game.prototype.init = function () {
    // type, health, damage, price, title, des, id
	// price = hp + damage - 2;
    let base = Date.now() % 10000;
    this.cards.push (new Card (1, 1, 1, 0, "SCV", "Light unit used to construct buildings, gather resources, and perform repairs.", base++));
    this.cards.push (new Card (2, 2, 1, 1, "MARINE", "Well-rounded unit suited for a wide range of combat situations.", base++));
    this.cards.push (new Card (3, 1, 2, 1, "REAPER", "Very fast raider that can jump over ledges, which also afford them great mobility.", base++));
    this.cards.push (new Card (4, 2, 2, 2, "MARAUDER", "Armored assault trooper that can attack ground targets.", base++));
    this.cards.push (new Card (5, 3, 1, 2, "HELLION", "LIght vehicle mounted flamethrower damages all units in a straight line.", base++));
    this.cards.push (new Card (6, 1, 3, 2, "GHOST", "Special ops unit that can eliminate biological targets using high-caliber Sniper rounds.", base++));
    this.cards.push (new Card (7, 3, 2, 3, "VIKING", "Slow-moving armored craft that can transform to attack air or ground targets.", base++));
    this.cards.push (new Card (8, 2, 3, 3, "THOR", "Its main cannons deal heavy damage to all land-based units and structures.", base++));
    this.cards.push (new Card (9, 3, 3, 4, "SIEGE TANK", "Provides devastating artillery strikes from long range when deployed in Siege Mode.", base++));
    this.cards.push (new Card (10, 4, 1, 3, "MEDIVAC", "Armored transport craft that has the ability to heal biological units.", base++));
	this.cards.push (new Card (1, 1, 1, 0, "SCV", "Light unit used to construct buildings, gather resources, and perform repairs.", base++));
	this.cards.push (new Card (2, 2, 1, 1, "MARINE", "Well-rounded unit suited for a wide range of combat situations.", base++));
	this.cards.push (new Card (3, 1, 2, 1, "REAPER", "Very fast raider that can jump over ledges, which also afford them great mobility.", base++));
	this.cards.push (new Card (4, 2, 2, 2, "MARAUDER", "Armored assault trooper that can attack ground targets.", base++));
	this.cards.push (new Card (5, 3, 1, 2, "HELLION", "LIght vehicle mounted flamethrower damages all units in a straight line.", base++));
	this.cards.push (new Card (6, 1, 3, 2, "GHOST", "Special ops unit that can eliminate biological targets using high-caliber Sniper rounds.", base++));
	this.cards.push (new Card (7, 3, 2, 3, "VIKING", "Slow-moving armored craft that can transform to attack air or ground targets.", base++));
	this.cards.push (new Card (8, 2, 3, 3, "THOR", "Its main cannons deal heavy damage to all land-based units and structures.", base++));
	this.cards.push (new Card (9, 3, 3, 4, "SIEGE TANK", "Provides devastating artillery strikes from long range when deployed in Siege Mode.", base++));
	this.cards.push (new Card (10, 4, 1, 3, "MEDIVAC", "Armored transport craft that has the ability to heal biological units.", base));
    this.shuffleArray (this.cards);
};

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm or Fisher-Yates (aka Knuth) Shuffle.
 */
Game.prototype.shuffleArray = function (array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor (Math.random () * (i + 1));
        let temp = array [i];
        array [i] = array [j];
        array [j] = temp;
    }
    return array;
};

module.exports = Game;